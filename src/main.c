#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>

#define REGION_SIZE 4096
#define heap_size1 100
#define heap_size2 100

static struct block_header* block_get_header(void* block)
{
    return (struct block_header*) (((uint8_t*)block) - offsetof(struct block_header, contents));
}

static void access_malloc_test()
{
    printf("Test 1, access allocate memory.\n");

    void *heap = heap_init(REGION_SIZE);
    assert(heap);

    void* block = _malloc(16);
    struct block_header* header = block_get_header(block);
    assert(header->is_free);
    assert(header->capacity.bytes == 24);
    _free(block);
    heap_term();

    printf("Test 1 successful.\n\n");
}

static void single_free_test()
{
    printf("Test 2, free one block.\n");

    void* heap = heap_init(REGION_SIZE);
    assert(heap != NULL);

    void* content1 = _malloc(heap_size1);
    assert(content1 != NULL);
    void* content2 = _malloc(heap_size2);
    assert(content2 != NULL);
    _free(content2);

    struct block_header* header1 = block_get_header(content1);
    struct block_header* header2 = block_get_header(content2);

    _free(content1);
    assert(header1->is_free);
    assert(header2->is_free);
    assert(header1->next == header2);

    heap_term();

    printf("Test 2 successful.\n\n");
}

static void multiple_free_test()
{
    printf("Test 3, free multiple blocks.\n");

    void* heap = heap_init(4096);
    assert(heap != NULL);
    void* content1 = _malloc(100);
    assert(content1 != NULL);
    void* content2 = _malloc(120);
    assert(content2 != NULL);
    void* content3 = _malloc(150);
    assert(content3 != NULL);
    void* content4 = _malloc(100);
    assert(content4 != NULL);
    _free(content2);
    _free(content4);

    struct block_header* header1 = block_get_header(content1);
    struct block_header* header2 = block_get_header(content2);
    struct block_header* header3 = block_get_header(content3);
    struct block_header* header4 = block_get_header(content4);

    assert(header1->next == header2);
    assert(header2->is_free);
    assert(header3->next == header4);
    assert(header4->is_free);

    _free(content1);
    _free(content3);
    heap_term();

    printf("Test 3 successful.\n\n");
}

static void grow_new_region_test()
{
    printf("Test 4, grow new region.\n");

    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap != NULL);

    void* content = _malloc(REGION_MIN_SIZE);
    assert(content != NULL);

    struct block_header* header = block_get_header(content);
    assert(header->capacity.bytes == REGION_MIN_SIZE);

    _free(content);
    heap_term();

    printf("Test 4 successful.\n\n");
}

static void new_region_grow_in_another_place_test() {
    printf("Test 5, grow new region in another place.\n");

    void* heap = heap_init(0);
    void* content = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    void* space = mmap(HEAP_START + REGION_MIN_SIZE, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, -1, 0);

    assert(heap);
    assert(content);
    assert(space);

    void* separate_content = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    assert(content);
    assert(separate_content);

    struct block_header* separate_header = block_get_header(separate_content);
    munmap(space, 24);

    struct block_header* header = (struct block_header*) heap;

    assert(header->next == separate_header);
    assert((void*) header + offsetof(struct block_header, contents) + header->capacity.bytes != separate_header);


    _free(content);

    heap_term();

    printf("Test 5 successful.\n\n");
}


int main()
{
    access_malloc_test();
    single_free_test();
    multiple_free_test();
    grow_new_region_test();
    new_region_grow_in_another_place_test();

    return 0;
}
